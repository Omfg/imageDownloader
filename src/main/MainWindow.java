package main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by omfg on 03.12.16.
 */
public class MainWindow extends JFrame {

   private JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
    private JPanel panel1 = new JPanel(null);
    private JPanel panel2 = new JPanel(null);
    private JTextArea area = new JTextArea("Enter url here...");
    private JButton getImage = new JButton("Get Image");
    private JButton getFile = new JButton("Get File");
    private JButton btnView = new JButton("View");


    private Choice choice = new Choice();
    private JLabel label = new JLabel("format");

    //Меню
    private JMenuBar menuBar = new JMenuBar();
    private JMenu menuFile = new JMenu();
    private JMenu menuOptions = new JMenu();

    private JMenuItem menuItem1 = new JMenuItem();
    private JMenuItem menuItem2 = new JMenuItem();
    private JPanel imagePanel = new JPanel();
    private JScrollPane scrollPane = new JScrollPane();
    private JLabel imageLabel = new JLabel();


    public MainWindow(int width, int height)  {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setSize(width,height);

        getContentPane().setLayout(null);
        setResizable(false);
//        setVisible(true);
//Объявление всякой фигни



        //Конец





//Указание границ и размеров


        scrollPane.setBounds(10,40,580,320);


        tabbedPane.setBounds(10,40,605,400);
        area.setBounds(10,50,600,320);
        area.setSize(600,320);
        getFile.setBounds(430,10,150,30);
        getImage.setBounds(10,10,150,30);
        choice.setBounds(520,10,70,20);
        label.setBounds(450,10,70,15);
        btnView.setBounds(10,10,90,25);
        menuBar.setBounds(0,0,640,30);




        //добавление элементов в панели

        getContentPane().add(tabbedPane);
        tabbedPane.addTab("Tab1",panel1);
        tabbedPane.addTab("Tab2",panel2);
        scrollPane.setViewportView(imagePanel);

        imagePanel.add(imageLabel,BorderLayout.CENTER);
        imageLabel.setText("TEST");



        //Панель 1

        panel1.add(getImage);
        panel1.add(getFile);
        panel1.add(area);


        ;
        //Панель 2
        panel2.add(choice);
        panel2.add(label);
        panel2.add(btnView);
        panel2.add(scrollPane);


//Choise
        choice.add("jpg");
        choice.add("png");

        //Menu

        getContentPane().add(menuBar);
        menuBar.add(menuFile);
        menuBar.add(menuOptions);
        menuFile.setText("File");
        menuOptions.setText("Options");
        menuItem1.setText("Save Image");
        menuItem2.setText("Exit");
        menuFile.add(menuItem1);
        menuOptions.add(menuItem2);

        //menu bar update
        menuBar.updateUI();




        // Action Listners
        getImage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {

                    Main.setImage(new URL(area.getText()));
                    System.out.println(Main.getImage());

                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                    JOptionPane.showMessageDialog(null,"Error reading url");
                }


            }
        });
        btnView.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Main.getImage()==null){
                    JOptionPane.showMessageDialog(null,"Image not Loaded");
                    return;
                }
                System.out.println(1);
                imageLabel.setIcon(new ImageIcon(Main.getImage()));
                imageLabel.updateUI();

            }
        });


setVisible(true);
    }





}
